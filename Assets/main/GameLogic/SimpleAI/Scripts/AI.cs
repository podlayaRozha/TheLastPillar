﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class AI : MonoBehaviour {
	
	private GameObject[] points;
	private int destPoint = 0;
	private NavMeshAgent agent;
	private float distPassed;

	void Start () {
		agent = GetComponent<NavMeshAgent>();

		points = GameObject.FindGameObjectsWithTag ("Waypoint").OrderBy(go => go.name).ToArray();
		agent.autoBraking = false;
		GotoNextPoint();
	}


	void GotoNextPoint() {
		//print ("Dest: " + points[destPoint].name);
		if (points.Length == 0) 
			return;

		agent.destination = points[destPoint].transform.position;

		destPoint = (destPoint + 1) % points.Length;
	}


	void Update () {
		
		distPassed = Mathf.Abs (agent.remainingDistance - distPassed);
		//print (distPassed);
		if (agent.remainingDistance < 0.5f)
		{
			GotoNextPoint();
		}

		distPassed = agent.remainingDistance;
	}
}
