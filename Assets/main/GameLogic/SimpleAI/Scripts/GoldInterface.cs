﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldInterface : MonoBehaviour {

	public int StartGold;
	public Text GoldAmountUI;
	public float goldAmount { get; private set;}

	void Start()
	{
		goldAmount = StartGold;
	}

	void ReciveGold(float amount)
	{
		//print ("Recive:" + amount);
		goldAmount += amount;
	}

	public bool TakeGold(float amount)
	{
		print ("Amount:" + amount);
		if (goldAmount - amount >= 0) {
			goldAmount -= amount;
			return true;
		}
		return false;
	}

	void OnGUI()
	{
		GoldAmountUI.text = "Gold: " + goldAmount;
	}
}
