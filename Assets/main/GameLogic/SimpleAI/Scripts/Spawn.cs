﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {
	public int max;
	public int spawnDelay;
	int currentCount;
	public Transform SpawnPoint;

	[System.Serializable]
	public class BotFactory{
		public Transform prefab;
		[Range(0, 1f)]
		public float chance;

		public void SpawnObject(Transform SpawnPoint)
		{
			//var SpawnPoint = GameObject.FindWithTag("Respawn");
			Instantiate(prefab, SpawnPoint.transform.position, Quaternion.identity);
		}
	}

	public BotFactory[] factory;

	// Use this for initialization
	void Start () {
		StartCoroutine (StartSpawn());
	}


	 IEnumerator StartSpawn () {
		for(int i = 0; i < factory.Length; i++) {
			if (currentCount < max) {
				if (Random.value < factory [i].chance) {
					factory [i].SpawnObject (SpawnPoint);
					currentCount++;
				}
			}
			yield return new WaitForSeconds (spawnDelay);
		}

		try {
			if(currentCount < max)
				StartCoroutine (StartSpawn());
		} catch (System.Exception ex) {
			print ("Error:" + ex.Message);
		}
	}
}
