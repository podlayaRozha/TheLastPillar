﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalSystem : MonoBehaviour {
	
	public int maxHealth = 10;
	public Slider HPSlider;
	public Text HPText;
	public GameObject gameOVerScreen;

	public int currentHealth {
		get;
		private set;
	}

	void Start()
	{
		currentHealth = maxHealth;
	}

	void OnTriggerEnter(Collider hit){
		
		if (currentHealth < 1) {
			gameOVerScreen.gameObject.SetActive (true);
			Time.timeScale = 0.2f;
			return;
		}

		if (hit.gameObject.tag == "Bot") {
			currentHealth--;
			Destroy (hit.gameObject);
		}

		HPSlider.value = currentHealth;
		HPText.text = currentHealth.ToString () + "/" + maxHealth;

	}		
}
