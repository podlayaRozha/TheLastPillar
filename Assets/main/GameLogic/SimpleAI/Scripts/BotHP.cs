﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotHP : MonoBehaviour {

	public int maxHealth;
	public Slider HPSlider;
	public int GoldCost;
	private int currentHealth;
	Animation anim;
	GameObject player;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animation> ();
		currentHealth = maxHealth;
		HPSlider.maxValue = maxHealth;
		HPSlider.value = currentHealth;
		player = GameObject.Find ("Player");
	}

	void OnDamageRecived(int damage)
	{			
		currentHealth -= damage;

		if (currentHealth < 1) {
			player.SendMessage ("ReciveGold", GoldCost);
			Destroy (this.gameObject);
		}
				
		HPSlider.value = currentHealth;
		print ("Health:" + currentHealth.ToString());

	}

	void OnGUI()
	{
		HPSlider.gameObject.transform.LookAt (player.transform);
	}
}
