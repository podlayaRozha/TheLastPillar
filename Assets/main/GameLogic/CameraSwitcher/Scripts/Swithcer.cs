﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Swithcer : MonoBehaviour {
	public float xOffset;
	public float zOffset;
	public float delay;
	public ParticleSystem teleportParticleEffect;
	public ParticleSystem towerTeleport;
	private GameObject center;

	void Start()
	{
		center = GameObject.FindGameObjectWithTag("Focus");
	}

	public void ChangeCamera()
	{
		teleportParticleEffect.Play ();
		towerTeleport.Play ();

		StartCoroutine (Teleport());

	}

	IEnumerator Teleport()
	{
		yield return new WaitForSeconds (delay);
		towerTeleport.Stop ();
		var camera = GameObject.Find ("Player");
		Vector3 newPos = new Vector3 (this.transform.position.x + xOffset, camera.transform.position.y, this.transform.position.z + zOffset);
		camera.gameObject.transform.position = newPos;
		camera.gameObject.transform.position.Set (newPos.x, newPos.y, newPos.z);
		camera.gameObject.transform.LookAt (center.transform);
		teleportParticleEffect.Stop ();
	}
}
