﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotMenu : MonoBehaviour {

	[System.Serializable]
	public struct PlacableTower
	{
		public GameObject prefab;
		public float price;
	}
		
	public PlacableTower[] Towers;
	public Transform placePosition;
	TextControls NotEnoughGoldText;
	GoldInterface goldController;

	void Start()
	{
		goldController = GameObject.Find("Player").GetComponent <GoldInterface> ();
		NotEnoughGoldText = GameObject.Find ("NotEnoughGoldText").GetComponent<TextControls> ();
		//print (goldController.goldAmount);
	}


	void OnGUI () {
		//this.transform.LookAt (GameObject.Find("Player").transform);
		var lookPos = GameObject.Find("Player").transform.position - transform.position;
		lookPos.y = 0;
		var rotation = Quaternion.LookRotation(lookPos);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 1);
	}

	public void PlaceTower(int towerType)
	{
		
		print ("price:" + Towers [towerType].price);

		if (goldController.TakeGold (Towers [towerType].price)) {
			var tower = Instantiate (Towers [towerType].prefab, placePosition.position, Quaternion.identity);
			tower.transform.position = placePosition.position;
			Destroy (this.gameObject);
		} else
			StartCoroutine (NotEnoughGoldText.FadeText(3f));
	}
			
}
