﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerMenu : MonoBehaviour {

	public int NumOfLvl;
	public int DamageIncrement;
	public int ReloadDecriment;
	public int UpgradePrice;
	public Canvas Canvas;
	public GameObject UIOnject;
	TextControls NotEnoughGoldText;
	int currentLvl;
	TowerAttack tower;
	GoldInterface goldController;

	void Start()
	{
		tower = GetComponent<TowerAttack> ();
		goldController = GameObject.Find("Player").GetComponent <GoldInterface> ();
		NotEnoughGoldText = GameObject.Find ("NotEnoughGoldText").GetComponent<TextControls> ();
		currentLvl = 1;
	}

	public void Upgrade()
	{
		if (currentLvl > NumOfLvl - 1)
			return;
		
		if (!goldController.TakeGold (UpgradePrice)) 
		{	
			StartCoroutine (NotEnoughGoldText.FadeText(3f));
			UIOnject.SetActive (false);
			return;
		}
		
		tower.damageAmount += DamageIncrement;
		tower.damageDelay -= ReloadDecriment;
		currentLvl++;

		UIOnject.SetActive (false);
	}

	public void ToggleInterface()
	{
		UIOnject.SetActive (!UIOnject.activeSelf);
	}


	void OnGUI () {
		Canvas.transform.LookAt (GameObject.Find("Player").transform);
	}

}
