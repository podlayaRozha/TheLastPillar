﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttack : MonoBehaviour {
	public int damageDelay;
	public float damageAmount;
	public GameObject towerHead;
	Queue <GameObject> target = new Queue<GameObject>();
	float timer;

	void Update()
	{
		timer += Time.deltaTime;

		if (timer >= damageDelay & target.Count != 0)
			AttackTarget ();
	}

	void AttackTarget()
	{
		try {
			
			var currentTarget = target.Peek ();

			currentTarget.SendMessage ("OnDamageRecived", damageAmount);

			timer = 0f;

			towerHead.gameObject.transform.LookAt (currentTarget.transform);

		} catch (System.Exception ex) {
			target.Dequeue ();
			return;
		}
	}		

	void OnTriggerEnter(Collider hit)
	{
		if (hit.gameObject.tag == "Bot") {
			target.Enqueue(hit.gameObject);
		}
	}

	void OnTriggerExit(Collider hit)
	{
		if (hit.gameObject.tag == "Bot") {
			target.Dequeue ();
		}
	}
}
