﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHide : MonoBehaviour {

	void InvokeHide()
	{
		this.gameObject.SetActive (false);
	}

	public void Hide()
	{
		Invoke ("InvokeHide", 1.75f);
	}
		

}
