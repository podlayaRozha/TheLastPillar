﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaweController : MonoBehaviour {
	public int timeBeforeStart;
	public Text timer;
	public string sayToPlayer;
	public GameObject[] spawn;

	float timeLeft;

	// Use this for initialization
	void Start () {
		StartCoroutine (waitBeforeStart());
	}

	IEnumerator waitBeforeStart()
	{
		
		while (timeLeft != timeBeforeStart) { 
			timeLeft++; 
			timer.text = sayToPlayer + (timeBeforeStart - timeLeft).ToString();	
			yield return new WaitForSeconds (1);
		}

		timer.text = "";

		foreach (var item in spawn) {
			item.gameObject.SetActive (true);
		}

	}

}
