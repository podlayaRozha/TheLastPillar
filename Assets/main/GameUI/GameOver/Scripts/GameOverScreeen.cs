﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScreeen : MonoBehaviour {
	private IEnumerator coChangeFoV;

	public void Restart(string sceneName)
	{
		ChangeFoV (1, 125);
		SceneManager.LoadSceneAsync (sceneName);
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadSceneAsync (0);
	}

	public void ChangeFoV (float duration, float value) {
		if (coChangeFoV != null) {
			StopCoroutine(coChangeFoV);
		}
		coChangeFoV = CoChangeFoV(duration, value);
		StartCoroutine(coChangeFoV);
	}

	IEnumerator CoChangeFoV(float duration, float value) {
		float t = 0.0f;
		float startFoV = Camera.main.fieldOfView;

		while (t != duration) {
			t += Time.deltaTime;
			if (t > duration) t = duration;
			Camera.main.fieldOfView = Mathf.Lerp(startFoV, value, t / duration);
			yield return null;
		}
	}
}
