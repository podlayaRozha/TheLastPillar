﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour {

	public void LoadMainMenu()
	{
		SceneManager.LoadSceneAsync (0);
	}

	public void ToggleMenu(bool state)
	{
		var btnImage = GetComponent<Image> ();
		var btnText = transform.GetChild (0).gameObject;
		btnText.SetActive (state);
		btnImage.enabled = state;
	}

	public void OpenGameMenu(GameObject Menu)
	{
		Menu.SetActive (true);
	}

	public void CloseGameMenu()
	{
		this.gameObject.SetActive (false);
	}

	public void LoadLevel(int lvl)
	{
		SceneManager.LoadScene (lvl);
	}
}
