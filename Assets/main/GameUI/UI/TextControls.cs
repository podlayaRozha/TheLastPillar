﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextControls : MonoBehaviour {

	Text UI;

	void Start()
	{
		UI = GetComponent<Text> ();
	}

	public IEnumerator FadeText(float time)
	{
		UI.gameObject.SetActive (true);
		UI.color = new Color(UI.color.r, UI.color.g, UI.color.b, 1);

		while (UI.color.a > 0.0f)
		{
			UI.color = new Color(UI.color.r, UI.color.g, UI.color.b, UI.color.a - (Time.deltaTime / time));
			yield return null;
		}

		UI.gameObject.SetActive (false);
	}

}
