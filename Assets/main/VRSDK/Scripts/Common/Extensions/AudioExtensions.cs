﻿using UnityEngine;
using System.Collections;

namespace Utils.Extensions
{
	public static class AudioExtensions {

		public static IEnumerator FadeOut (this AudioSource audioSource, float FadeTime, float startVolume) {			
			while (audioSource.volume > 0) {
				audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

				yield return null;
			}

			audioSource.Stop ();
			audioSource.volume = startVolume;
		}

		public static IEnumerator FadeOut (this AudioSource audioSource, float FadeTime) {
			float startVolume = audioSource.volume;
			return FadeOut (audioSource, FadeTime, startVolume);
		}			

		public static IEnumerator FadeIn (this AudioSource audioSource, float FadeTime, float endVolume) {
			audioSource.volume = 0;
			audioSource.Play ();
			while (audioSource.volume < endVolume) {
				audioSource.volume += endVolume * Time.deltaTime / FadeTime;
				yield return null;
			}			
			audioSource.volume = endVolume;
		}

		public static IEnumerator FadeIn (this AudioSource audioSource, float FadeTime) {
			float endVolume = audioSource.volume;
			return FadeIn (audioSource, FadeTime, endVolume);
		}

	}
}
