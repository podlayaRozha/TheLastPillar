﻿using System;
using UnityEngine;

namespace Utils.Extensions
{
	public class AngleExtensions
	{
		/// <summary>
		/// Convert angle to negative angle if it more thar 180 degree. 
		/// Example: 320 will converted to -40
		/// </summary>
		/// <returns>Converted angle.</returns>
		/// <param name="angle">Angle.</param>
		static public float ConvertToNegative(float angle)
		{
			if (angle > 180)
				angle -= 360;
			return angle;
		}

        static public float ClampAngle(float angle, float min , float max )
        {
 
             if (angle<90 || angle>270){       // if angle in the critic region...
                 if (angle>180) angle -= 360;  // convert all angles to -180..+180
                 if (max>180) max -= 360;
                 if (min>180) min -= 360;
             }
             angle = Mathf.Clamp(angle, min, max);
             if (angle<0) angle += 360;  // if angle negative, convert to 0..360
             return angle;
        }

        static public float WrapNumber(float input) // replace int with whatever your type is
        {
            // this will always return an angle between 0 and 360:
            // the inner % 360 restricts everything to +/- 360
            // +360 moves negative values to the positive range, and positive ones to > 360
            // the final % 360 caps everything to 0...360
            return ((input % 360) + 360) % 360;
        }
    }
}

