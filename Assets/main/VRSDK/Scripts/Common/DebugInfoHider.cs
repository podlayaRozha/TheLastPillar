﻿using UnityEngine;
using System.Collections;
using VRSDK;

[RequireComponent(typeof(VRInput))]
public class DebugInfoHider : MonoBehaviour {

	[SerializeField] GameObject _debugInfoObject;

	// Use this for initialization
	void Start () {
		if (_debugInfoObject == null)
			throw new UnityException ("Debug Info Object don't set in " + gameObject.name);

		var vrInput = GetComponent<VRInput> ();
		vrInput.OnSwipe += VrInput_OnSwipe;
	}

	void VrInput_OnSwipe (VRInput.SwipeDirection obj)
	{
		if (obj == VRInput.SwipeDirection.DOWN)
			_debugInfoObject.SetActive (true);
		else if (obj == VRInput.SwipeDirection.UP)
			_debugInfoObject.SetActive (false);
	}		
}
