﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FpsCounter : MonoBehaviour
{
	private int _frameCount;
	private float _dt;
	private float _fps;
	private float _updatePerSec = 2.0f;

    private Text _targetText;
    private Text _scenesName;
    // Use this for initialization
    void Start()
    {
        _targetText = GetComponent<Text>();
        _scenesName = GetComponent<Text>();
    }


    private void Update()
	{
		_frameCount++;
		_dt += Time.deltaTime;
		if (_dt > 1.0f/_updatePerSec)
		{
			_fps = _frameCount/_dt;
			_frameCount = 0;
			_dt -= 1.0f/_updatePerSec;
			_targetText.text = "FPS: " + String.Format("{0: 00}", _fps);
		}
	}
}