using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using DG.Tweening;

namespace VRSDK
{
    // This class is used to control a radial bar that fills
    // up as the user holds down the Fire1 button.  When it has
    // finished filling it triggers an event.  It also has a
    // coroutine which returns once the bar is filled.
    public class SelectionRadial : MonoBehaviour
    {
        public event Action OnSelectionComplete;                                                // This event is triggered when the bar has filled.

        [SerializeField] private float m_SelectionDuration = 1f;                                // How long it takes for the bar to fill.
        [SerializeField] private bool m_HideOnStart = true;                                     // Whether or not the bar should be visible at the start.
		[SerializeField] public Image m_Selection;                                             // Reference to the image who's fill amount is adjusted to display the bar.        
        [SerializeField] private Reticle m_Recticle; 
        [SerializeField] private VRInput m_VRInput;                                             // Reference to the VRInput so that input events can be subscribed to.        
        [SerializeField] private bool m_useButton = true;                                       // User must press OK button, to start feeling bar
		[SerializeField] private float _hideScale = 0.2f;	        	
		private float _hideTime = 0.2f;
        private Coroutine m_hideCoroutine;
        private float _startSelectionScale;
        private Coroutine m_SelectionFillRoutine;                                               // Used to start and stop the filling coroutine based on input.
        private bool m_IsSelectionRadialActive;                                                    // Whether or not the bar is currently useable.
                

        public float SelectionDuration
        {
            get { return m_SelectionDuration;}
        }


        private void OnEnable()
        {
			m_VRInput.OnDown += HandleDown;
			m_VRInput.OnUp += HandleUp;
        }
			
        private void OnDisable()
        {
			m_VRInput.OnDown -= HandleDown;
			m_VRInput.OnUp -= HandleUp;
        }


        private void Start()
        {
            m_Selection.fillAmount = 0f;
            _startSelectionScale = m_Selection.transform.localScale.x;
			if (m_HideOnStart) {
				Hide ();
			}
        }


        public void Show()
		{ 
			StartCoroutine(ShowCoroutine());
        }

        public IEnumerator ShowCoroutine()
		{ 
			Reset ();
            if(m_hideCoroutine != null)//если запущен эффект "сворачивания" кружка с выбором
                StopCoroutine(m_hideCoroutine);//останавливаем его
            Debug.Log("Called SetActive(true)");
            m_Selection.gameObject.SetActive(true);     
            if(m_Recticle != null){
                m_Recticle.Hide();
                yield return m_Selection.transform.DOScale(_startSelectionScale,_hideTime).WaitForCompletion();                
            }            
                        
            if(m_Recticle != null)
                m_Recticle.Hide();

			m_IsSelectionRadialActive = true;			
			if (!m_useButton) {//если стоит режим "без кнопки"
				HandleDown ();//симулируем нажатие кнопки, при наведение взгляда
			}
        }

		public void Reset()
		{
			if (m_SelectionFillRoutine != null) {
				StopCoroutine (m_SelectionFillRoutine);
			}
			m_Selection.fillAmount = 0f;
		}

        public void Hide()
        {						            
            m_hideCoroutine = StartCoroutine(HideCoroutine());          
        }

        private IEnumerator HideCoroutine(){
            if(m_Recticle != null){
                yield return m_Selection.transform.DOScale(_hideScale,_hideTime).WaitForCompletion();
                m_Recticle.Show();
            }
            Debug.Log("Called SetActive(false)");
            m_Selection.gameObject.SetActive(false);            


			if (!m_useButton)//если стоит режим "без кнопки"
				HandleUp ();//симулируем что кнопка была отжата, когда убираем взгляд
            m_IsSelectionRadialActive = false;
            // This effectively resets the radial for when it's shown again.
            m_Selection.fillAmount = 0f;  
            m_hideCoroutine = null;   
        }


        private IEnumerator FillSelectionRadial()
        {			            
            // Create a timer and reset the fill amount.
            float timer = 0f;
            m_Selection.fillAmount = 0f;
            
            // This loop is executed once per frame until the timer exceeds the duration.
            while (timer < m_SelectionDuration)
            {
                // The image's fill amount requires a value from 0 to 1 so we normalise the time.
                m_Selection.fillAmount = timer / m_SelectionDuration;

                // Increase the timer by the time between frames and wait for the next frame.
                timer += Time.deltaTime;
                yield return null;
            }

            // When the loop is finished set the fill amount to be empty.
            m_Selection.fillAmount = 0f;

            if(!m_useButton) //если стоит режим управления взглядом
                m_IsSelectionRadialActive = false;//сбрасываем флаг, объект нельзя было выбрать два раза подряд
            //иначе флаг остается равным true. Кнопкой можно выбрать объект второй раз            

            // If there is anything subscribed to OnSelectionComplete call it.			
			if (OnSelectionComplete != null) {				
				OnSelectionComplete();
			}
        }

        public void HandleDown()
        {
            // If the radial is active start filling it.
            if (m_IsSelectionRadialActive)
            {
                m_SelectionFillRoutine = StartCoroutine(FillSelectionRadial());
            }
        }


        public void HandleUp()
        {		
            // If the radial is active stop filling it and reset it's amount.
            if (m_IsSelectionRadialActive)
            {
                if(m_SelectionFillRoutine != null)
                    StopCoroutine(m_SelectionFillRoutine);

                m_Selection.fillAmount = 0f;
            }
        }
    }
}