﻿using UnityEngine;
using UnityEngine.VR;
using Utils.Extensions;

namespace VRSDK
{
    public class VRSimpleMouseLook : MonoBehaviour
    {
        [SerializeField]
        KeyCode HorizontalAndVerticalKey = KeyCode.LeftAlt;

        [SerializeField]
        float _mouseSensitivity = 0.5f;

        Transform _vrCameraTransform;

        float _mouseY = 0.0f; // rotation around the up/y axis
        float _mouseX = 0.0f; // rotation around the right/x axis

        void Awake()
        {
            _vrCameraTransform = GameObject.FindWithTag("MainCamera").transform;
            if (_vrCameraTransform == null)
                Debug.Log("Object with tag \"MainCamera\" is not found, SimpleMouseLook script is not operational.");
            else
            {
                Debug.Log("MouseLook script is operational.");
            }

            _mouseY = AngleExtensions.ConvertToNegative(transform.localEulerAngles.x);
            _mouseX = AngleExtensions.ConvertToNegative(transform.localEulerAngles.y);

        }

        void Update()
        {
            if (Input.GetKey(HorizontalAndVerticalKey))
            {
                _mouseX += Input.GetAxis("Mouse X")*_mouseSensitivity;//5;
                if (_mouseX <= -180)
                {
                    _mouseX += 360;
                }
                else if (_mouseX > 180)
                {
                    _mouseX -= 360;
                }

                _mouseY -= Input.GetAxis("Mouse Y")*_mouseSensitivity/2f;//2.4f;
                _mouseY = Mathf.Clamp(_mouseY, -85, 85);

                transform.localRotation = Quaternion.Euler(_mouseY, _mouseX, 0);
            }

        }
    }
}
