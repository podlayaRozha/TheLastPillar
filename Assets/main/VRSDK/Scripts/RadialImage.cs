﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace VRSDK
{
	public class RadialImage : MonoBehaviour {

		[SerializeField] private float _hideScale = 0.2f;		

		private float _hideTime = 0.2f;
		private float _startScale;
		// Use this for initialization
		void Start () {
			_startScale = transform.transform.localScale.x;
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		public void Hide(){
			StartCoroutine(HideCoroutine());
		}

		public void Show(){
			StartCoroutine(ShowCoroutine());
		}

		public IEnumerator HideCoroutine(){
			 yield return transform.DOScale(_hideScale, _hideTime);
		}

		public IEnumerator ShowCoroutine(){
			yield return transform.DOScale(_startScale, _hideTime);
		}
	}
}
