﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace VRSDK
{
    // This class works similarly to the SelectionRadial class except
    // it has a physical manifestation in the scene.  This can be
    // either a UI slider or a mesh with the SlidingUV shader.  The
    // functions as a bar that fills up whilst the user looks at it
    // and holds down the Fire1 button.
    //
    public class SelectionSlider : MonoBehaviour
    {
        public event Action OnBarFilled;                                    // This event is triggered when the bar finishes filling.

        [SerializeField] private float m_Duration = 2f;                     //время заполнение бара
        [SerializeField] private VRInteractiveItem m_InteractiveItem;       // ссылка на итем, чтобы знать когда заполнять бар
        [SerializeField] private VRInput m_VRInput;                         // для обнаружения нажатия кнопок
        [SerializeField] private GameObject m_BarCanvas;                    // Optional reference to the GameObject that holds the slider (only necessary if DisappearOnBarFill is true).
        [SerializeField] private SelectionRadial m_SelectionRadial;         // Optional reference to the SelectionRadial, if non-null the duration of the SelectionRadial will be used instead.
        //[SerializeField] private UIFader m_UIFader;                         // Optional reference to a UIFader, used if the SelectionSlider needs to fade out.
        [SerializeField] private Collider m_Collider;                       // Optional reference to the Collider used to detect the user's gaze, turned off when the UIFader is not visible.
        [SerializeField] private bool m_DisableOnBarFill;                   // когда бар заполнится, он останавливается
        [SerializeField] private bool m_DisappearOnBarFill;                 // когда бар заполнен, он изчезает


        private bool m_BarFilled;                                           // бар заполняется ли в настоящее время
        private bool m_GazeOver;                                            // смотрит ли сейчас пользователь на 
        private float m_Timer;                                              // используется чтобы узнать на сколько бар должен быть заполнен
        private Coroutine m_FillBarRoutine;                                 // Ссылка на сопрограмма, которая управляет баром заполнения, используется, чтобы остановить его при необходимости.


        private const string k_SliderMaterialPropertyName = "_SliderValue"; // The name of the property on the SlidingUV shader that needs to be changed in order for it to fill.


        private void OnEnable ()
        {
            m_VRInput.OnDown += HandleDown;
            m_VRInput.OnUp += HandleUp;

            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
        }


        private void OnDisable ()
        {
            m_VRInput.OnDown -= HandleDown;
            m_VRInput.OnUp -= HandleUp;

            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
        }


        private void Update ()
        {
            /*if(!m_UIFader)
                return;

            // If this bar is using a UIFader turn off the collider when it's invisible.
            m_Collider.enabled = m_UIFader.Visible;
        */
        }


        public IEnumerator WaitForBarToFill ()
        {
            // If the bar should disappear when it's filled, it needs to be visible now.
            //если бар должен изчезнуть когда наполнится, то он должен быть виден сейчас
            if(m_BarCanvas && m_DisappearOnBarFill)
                m_BarCanvas.SetActive(true);

            // в настоящее время бар не заполнен
            m_BarFilled = false;

            // Reset the timer and set the slider value as such.
            m_Timer = 0f;

            // Keep coming back each frame until the bar is filled.
            while (!m_BarFilled)
            {
                yield return null;
            }

            // If the bar should disappear once it's filled, turn it off.
            if (m_BarCanvas && m_DisappearOnBarFill)
                m_BarCanvas.SetActive(false);
        }


        private IEnumerator FillBar ()
        {
            // When the bar starts to fill, reset the timer.
            m_Timer = 0f;

            // The amount of time it takes to fill is either the duration set in the inspector, or the duration of the radial.
            float fillTime = m_SelectionRadial != null ? m_SelectionRadial.SelectionDuration : m_Duration;

            // Until the timer is greater than the fill time...
            while (m_Timer < fillTime)
            {
                // ... add to the timer the difference between frames.
                m_Timer += Time.deltaTime;

                // Wait until next frame.
                yield return null;

                // If the user is still looking at the bar, go on to the next iteration of the loop.
                if (m_GazeOver)
                    continue;

                // If the user is no longer looking at the bar, reset the timer and bar and leave the function.
                m_Timer = 0f;
                yield break;
            }

            // If the loop has finished the bar is now full.
            m_BarFilled = true;

            // If anything has subscribed to OnBarFilled call it now.
            if (OnBarFilled != null)
                OnBarFilled ();

            // If the bar should be disabled once it is filled, do so now.
            if (m_DisableOnBarFill)
                enabled = false;
        }

        private void HandleDown ()
        {
            // If the user is looking at the bar start the FillBar coroutine and store a reference to it.
            if (m_GazeOver)
                m_FillBarRoutine = StartCoroutine(FillBar());
        }


        private void HandleUp ()
        {
            // If the coroutine has been started (and thus we have a reference to it) stop it.
            if(m_FillBarRoutine != null)
                StopCoroutine (m_FillBarRoutine);

            // Reset the timer and bar values.
            m_Timer = 0f;
        }


        private void HandleOver()
        {
            // The user is now looking at the bar.
            m_GazeOver = true;
        }

        private void HandleOut ()
        {
            // The user is no longer looking at the bar.
            m_GazeOver = false;

            // If the coroutine has been started (and thus we have a reference to it) stop it.
            if (m_FillBarRoutine != null)
                StopCoroutine(m_FillBarRoutine);

            // Reset the timer and bar values.
            m_Timer = 0f;
        }
    }
}