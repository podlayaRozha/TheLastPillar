﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour {

	
	public void SetWhite(){
		var material = GetComponent<MeshRenderer>().material;
		material.color = Color.white;
	}

	public void SetBlue(){
		var material = GetComponent<MeshRenderer>().material;
		material.color = Color.blue;
	}
}
