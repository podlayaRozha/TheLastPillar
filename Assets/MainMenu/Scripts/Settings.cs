﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour {
	public Text currentLvlText;

	void FixedUpdate () {
		var lvl = QualitySettings.GetQualityLevel () + 1;
		currentLvlText.text = "Current preset: " + lvl.ToString ();
	}
}
