﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
	AsyncOperation async;
	float progress;
	private int pRounded;
	public GameObject settings;
	public GameObject main;
	public GameObject loadingScreen;

	public void openSettings ()
	{
		main.SetActive (false);
		settings.SetActive (true);
	}

	public void openMain()
	{
		main.SetActive (true);
		settings.SetActive (false);
	}

	public void startLoad()
	{
		StartCoroutine (sceneLoader());
	}

	public void setQuality(int lvl)
	{
		QualitySettings.SetQualityLevel (lvl);
	}

	IEnumerator sceneLoader(){
		AsyncOperation async = SceneManager.LoadSceneAsync(1);

		loadingScreen.gameObject.SetActive (true);
		main.SetActive (false);

		yield return true;
	}
}


